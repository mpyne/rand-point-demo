/*
 * Copyright © 2009 Michael Pyne <mpyne@purinchu.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "glview.h"

#include <QtOpenGL>
#include <QtGui/QSizePolicy>

#include "wireframe.h"

GLView::GLView (QWidget *parent)
  : QGLWidget (parent), m_wireframe (0)
{
  // The true parameter means that the height of the
  // widget depends on its width
  QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
  sizePolicy.setHeightForWidth(true);
  setSizePolicy(sizePolicy);
}

GLView::~GLView()
{
  delete m_wireframe;
}

void GLView::paintGL()
{
  glClear(GL_COLOR_BUFFER_BIT);

  if(m_wireframe)
    m_wireframe->draw_wireframe();
}

void GLView::resizeGL (int width, int height)
{
  glViewport (0, 0, width, height);

  glMatrixMode (GL_PROJECTION);
  glLoadIdentity ();
  glFrustum (-1000.0, 1000.0, -1000.0, 1000.0, 6000.0, 15000.0);
}

void GLView::initializeGL ()
{
  glClearColor (0.0, 0.0, 0.0, 1.0);
  glShadeModel (GL_SMOOTH);

  // Enable anti-aliasing
  glEnable (GL_LINE_SMOOTH);
  glEnable (GL_BLEND);
  glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void GLView::drawAxes (void)
{
}

void GLView::setWireframe(Wireframe *wireframe)
{
  delete m_wireframe;
  m_wireframe = wireframe;
  update();
}

/* vim: set et sw=2 ts=2: */
