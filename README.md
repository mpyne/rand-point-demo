# rand-point-demo

## Description

This is a program to display random data in a cube to illustrate some potential problems with random number
generators.  (The cube is a form of spectral analysis, which can be carried to higher dimensions as well).

## Version

Version: 1.0

## Author

By Michael Pyne (done years ago as part of a college project, ported over in a day to Qt 4)

## Notes

This requires Qt 4 in addition to Open GL along with a compilation toolchain
for Qt programs such as GCC/gmake.

There's no reason this shouldn't build (in theory at least) on Windows and Mac as well, although the build
instructions assume a UNIX-ish system.

## License

BSD style (see COPYING for details).

## Compiling/Running:

    $ tar xzf pointviewer-1.0.tar.gz && cd pointviewer-1.0
    $ qmake
    $ make
    $ ./pointviewer
