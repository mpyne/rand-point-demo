#ifndef _WIREFRAME_H
#define _WIREFRAME_H
/*
 * Copyright © 2009 Michael Pyne <mpyne@purinchu.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <QtCore/QVector>

class Point : public QVector<double>
{
  public:
  Point(const double &defaultValue = double()) : QVector<double>(3, defaultValue) { }
};

class QString;

class Wireframe
{
  public:
  Wireframe (const QString &fileName);
  Wireframe (const QVector<Point> &_points);
  Wireframe (const Wireframe &w);
  ~Wireframe ();

  enum Axis { XAxis, YAxis, ZAxis };

  void draw_wireframe (void);

  /* Move model such that every point is moved right by dx
   * and moved up by dy.  Negative values are allowed.
   */
  void translate (double dx, double dy, double dz);

  /* Scale model horizontally by sx and vertically by sy. |s| < 1.0
   * shrinks the model, s < 0.0 inverts that axis.  The scaling is
   * done so that (x,y) remains constant.
   */
  void scale (double sx, double sy, double sz,
              double x, double y, double z);

  /* Rotate the model by the given number of degrees, about the given
   * axis.  The rotation
   * occurs about (x,y).  Positive values give a counter-clockwise
   * rotation.
   */
  void rotate (double deg, Axis axis, double x, double y, double z);
  void rotate_only (double deg, Axis axis);
  void set_width (double w) { width = w; }

  private:
  QVector<Point> points;
  Point center;
  Point minValue;
  Point maxValue;
  int n_points;
  double distance, width;

  void calculate_bounding_box();
  void draw_bounding_box();
};

#endif /* _WIREFRAME_H */

/* vim: set et sw=2 ts=2: */
