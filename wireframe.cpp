/*
 * Copyright © 2009 Michael Pyne <mpyne@purinchu.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "wireframe.h"

#include <QtCore/QFile>
#include <QtCore/QTextStream>
#include <QtGui/QMessageBox>
#include <QtDebug>
#include <QtOpenGL>

#include <limits>

Wireframe::Wireframe (const QString &fileName)
  : n_points (0), width (1.0)
{
  QFile file(fileName);
  if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
    qDebug() << "Unable to open file" << fileName;
    QMessageBox::critical(0, "Unable to open file", QString("Unable to open file %1").arg(fileName));
    return;
  }

  QTextStream in(&file);

  in >> n_points;
  qDebug() << n_points << "points";

  // Set up points array
  points.resize(n_points);

  for (int i = 0; i < n_points; ++i)
  {
    in >> points[i][0];
    in >> points[i][1];
    in >> points[i][2];
  }

  calculate_bounding_box ();
  for (int i = 0; i < n_points; ++i)
  {
    points[i][0] -= center[0];
    points[i][1] -= center[1];
    points[i][2] -= center[2];
  }

  for (int i = 0; i < 3; ++i) {
    minValue[i] -= center[i];
    maxValue[i] -= center[i];
  }

  distance = 2000.0;
  glMatrixMode (GL_MODELVIEW);
  glLoadIdentity();
}

Wireframe::Wireframe(const QVector<Point> &_points)
  : points (_points), n_points(_points.size()),
    distance (2000.0), width(1.0)
{
  calculate_bounding_box ();
  for (int i = 0; i < n_points; ++i)
  {
    points[i][0] -= center[0];
    points[i][1] -= center[1];
    points[i][2] -= center[2];
  }

  for (int i = 0; i < 3; ++i) {
    minValue[i] -= center[i];
    maxValue[i] -= center[i];
  }

  glMatrixMode (GL_MODELVIEW);
  glLoadIdentity();
}

// Copy constructor
Wireframe::Wireframe (const Wireframe &w)
  : points (w.points), n_points (w.n_points),
  distance (w.distance), width (w.width)
{
  center[0] = w.center[0];
  center[1] = w.center[1];
  center[2] = w.center[2];
}

Wireframe::~Wireframe ()
{
}

void Wireframe::draw_wireframe (void)
{
  GLdouble color1[] = {
    1.0, 0.75, 0.5, /* Yuck 1 */
  };

  glMatrixMode (GL_MODELVIEW);
  glPushMatrix ();
  glTranslated (0.0, 0.0, -6000.0 - distance);

  /* Go through the point array, and draw the points */
  glColor3dv (color1);
  glBegin(GL_POINTS);

  for (int i = 0; i < n_points; i++)
    glVertex3dv (&points[i][0]);

  glEnd();

  // Now draw the bounding box
  draw_bounding_box ();

  glPopMatrix ();
}

void Wireframe::translate (double dx, double dy, double dz)
{
  glMatrixMode (GL_MODELVIEW);
  glTranslated (dx, dy, dz);
}

void Wireframe::scale (double sx, double sy, double sz,
                       double x, double y, double z)
{
  glMatrixMode (GL_MODELVIEW);
  glTranslated (0.0, 0.0, -6000.0 - distance);
  glScaled (sx, sy, sz);
  glTranslated (0.0, 0.0, 6000.0 + distance);

  (void) x;
  (void) y;
  (void) z;
}

void Wireframe::rotate_only (double deg, Axis axis)
{
  glMatrixMode (GL_MODELVIEW);
  switch (axis)
  {
    case ZAxis:
      glRotated (deg, 0, 0, 1);
    break;

    case YAxis:
      glRotated (deg, 0, 1, 0);
    break;

    case XAxis:
      glRotated (deg, 1, 0, 0);
    break;
  }
}

void Wireframe::rotate (double deg, Axis axis, double x, double y, double z)
{
  glTranslated (0.0, 0.0, -6000.0 - distance);
  rotate_only (deg, axis);
  glTranslated (0.0, 0.0, 6000.0 + distance);

  (void) x;
  (void) y;
  (void) z;
}

void Wireframe::calculate_bounding_box (void)
{
  for (int i = 0; i < 3; ++i) {
    minValue[i] = std::numeric_limits<double>::max();
    maxValue[i] = std::numeric_limits<double>::min();
  }

  for (int i = 0; i < n_points; ++i)
  {
    for (int j = 0; j < 3; ++j)
    {
      if (points[i][j] < minValue[j])
        minValue[j] = points[i][j];
      else if (points[i][j] > maxValue[j])
        maxValue[j] = points[i][j];
    }
  }

  for (int i = 0; i < 3; ++i)
    center[i] = minValue[i] + (maxValue[i] - minValue[i]) / 2;
}

void Wireframe::draw_bounding_box ()
{
  GLdouble lineColor[] = {
    0.33, 0.5, 0.7,
  };

  glColor3dv (lineColor);
  glBegin (GL_LINES);
    glVertex3d (maxValue[0], maxValue[1], maxValue[2]);
    glVertex3d (maxValue[0], minValue[1], maxValue[2]);

    glVertex3d (maxValue[0], maxValue[1], maxValue[2]);
    glVertex3d (minValue[0], maxValue[1], maxValue[2]);

    glVertex3d (maxValue[0], maxValue[1], maxValue[2]);
    glVertex3d (maxValue[0], maxValue[1], minValue[2]);

    glVertex3d (minValue[0], minValue[1], maxValue[2]);
    glVertex3d (maxValue[0], minValue[1], maxValue[2]);

    glVertex3d (minValue[0], minValue[1], maxValue[2]);
    glVertex3d (minValue[0], maxValue[1], maxValue[2]);

    glVertex3d (minValue[0], minValue[1], maxValue[2]);
    glVertex3d (minValue[0], minValue[1], minValue[2]);

    glVertex3d (maxValue[0], minValue[1], minValue[2]);
    glVertex3d (maxValue[0], minValue[1], maxValue[2]);

    glVertex3d (maxValue[0], minValue[1], minValue[2]);
    glVertex3d (maxValue[0], maxValue[1], minValue[2]);

    glVertex3d (maxValue[0], minValue[1], minValue[2]);
    glVertex3d (minValue[0], minValue[1], minValue[2]);

    glVertex3d (minValue[0], maxValue[1], minValue[2]);
    glVertex3d (minValue[0], maxValue[1], maxValue[2]);

    glVertex3d (minValue[0], maxValue[1], minValue[2]);
    glVertex3d (maxValue[0], maxValue[1], minValue[2]);

    glVertex3d (minValue[0], maxValue[1], minValue[2]);
    glVertex3d (minValue[0], minValue[1], minValue[2]);

    // Display axes
    glColor3f (1.0, 0.0, 0.0);
    glVertex3d (0.0, 0.0, 0.0);
    glVertex3d (1000.0, 0.0, 0.0);

    glColor3f (0.0, 1.0, 0.0);
    glVertex3d (0.0, 0.0, 0.0);
    glVertex3d (0.0, 1000.0, 0.0);

    glColor3f (0.0, 0.0, 1.0);
    glVertex3d (0.0, 0.0, 0.0);
    glVertex3d (0.0, 0.0, 1000.0);

  glEnd ();
}

/* vim: set et sw=2 ts=2: */
