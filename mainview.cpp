/*
 * Copyright © 2009 Michael Pyne <mpyne@purinchu.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "mainview.h"

#include <QtDebug>
#include <QtCore/QTimer>
#include <QtCore/QRegExp>
#include <QtCore/QFile>
#include <QtGui/QLabel>
#include <QtGui/QSlider>
#include <QtGui/QRadioButton>
#include <QtGui/QDoubleSpinBox>
#include <QtGui/QHBoxLayout>

#include <cmath>

#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "wireframe.h"
#include "ui_mainview.h"
#include "ui_generateDialog.h"
#include "glview.h"

MainView::MainView (QWidget *parent)
  : QWidget (parent), m_demo (new QTimer(this)), m_ui(new Ui::MainView)
{
  // Set up list of models
  m_ui->setupUi(this);

  QHBoxLayout *baseLayout = new QHBoxLayout(m_ui->base);
  baseLayout->setSpacing(0);
  baseLayout->setMargin(0);

  m_gl_view = new GLView (m_ui->base);
  baseLayout->addWidget(m_gl_view);

  m_ui->xAxis->setChecked(true);
  m_speed = m_ui->delay->value();

  connect (m_demo, SIGNAL(timeout()), this, SLOT(play_demo_step()));

  emit enableActionButtons(false); // Disable buttons
}

MainView::~MainView ()
{
}

void MainView::start_demo ()
{
  if (m_demo->isActive()) {
    stop_demo();
    return;
  }

  m_ui->demoButton->setText("Sto&p");
  m_demo->start(m_speed);
  last_t = 0.0;
}

void MainView::stop_demo ()
{
  m_demo->stop();
  m_ui->demoButton->setText("&Demo");
}

double to_deg (double x)
{
  return 180.0 * x / M_PI;
}

void MainView::play_demo_step ()
{
  double new_t = last_t + .00155;
  double theta;
  const double twopi = 2 * M_PI;

  using std::sin;

  theta = twopi * (sin(last_t) - sin(new_t));
  m_gl_view->wireframe()->rotate (to_deg(theta), Wireframe::ZAxis, 0, 0, 0);

  theta = twopi * M_SQRT1_2 * (sin(new_t - M_PI_4) - sin(last_t - M_PI_4));
  m_gl_view->wireframe()->rotate (to_deg(theta), Wireframe::XAxis, 0, 0, 0);

  theta = twopi * (sin(new_t) - sin(last_t));
  m_gl_view->wireframe()->rotate (to_deg(theta), Wireframe::XAxis, 0, 0, 0);

  m_gl_view->update();
}

void MainView::clear_models ()
{
  emit enableActionButtons(false);

  stop_demo();
  m_gl_view->setWireframe(0);
}

void MainView::rotate ()
{
  Wireframe::Axis axis;

  if(m_ui->xAxis->isChecked())
    axis = Wireframe::XAxis;
  else if(m_ui->yAxis->isChecked())
    axis = Wireframe::YAxis;
  else
    axis = Wireframe::ZAxis;

  m_gl_view->wireframe()->rotate (m_ui->rotationAmount->value(), axis, 0.0, 0.0, 0.0);
  m_gl_view->update();
}

void MainView::scale ()
{
  double scaleFactor = m_ui->scaleAmount->value();

  m_gl_view->wireframe()->scale (scaleFactor, scaleFactor, scaleFactor,
                0.0, 0.0, 0.0
               );
  m_gl_view->update();
}

void MainView::set_speed (int s)
{
  m_speed = s;
  if (m_demo)
    m_demo->setInterval (s);
}

static double randu()
{
  static quint32 last = 47; // Odd number chosen randomly -- seed guaranteed to be random :)
  static double divisor = quint32(~0) / 1000.0;

  last = last * 65539;
  return last / divisor;
}

static double urandom_rand()
{
  quint32 value = 0;
  static double divisor = quint32(~0) / 1000.0;
  static bool errored = false;
  static int fd = 0;

  if(fd == 0) {
    fd = open("/dev/urandom", O_RDONLY);
    if(fd < 0) {
      if (!errored) {
        qDebug() << "/dev/urandom does not work for some reason";
        errored = true;
      }

      return 0.0;
    }
  }

  if (read(fd, &value, sizeof value) < static_cast<int>(sizeof value)) {
    return -1.0;
  }

  return value / divisor;
}

static double system_rand()
{
  static double divisor = RAND_MAX / 1000.0;
  return random() / divisor;
}

void MainView::generate_data()
{
  QDialog dialog(this);
  Ui::GenerateDialog dialogUi;

  dialogUi.setupUi(&dialog);

  if(dialog.exec() != QDialog::Accepted)
    return;

  double (*random_fn)() = randu;
  int numPoints = dialogUi.numberPoints->value();
  switch(dialogUi.method->currentIndex()) {
    case 0: // RANDU
      random_fn = randu;
    break;

    case 1: // /dev/urandom
      random_fn = urandom_rand;
    break;

    case 2: // glibc
      random_fn = system_rand;
    break;

    default:
      random_fn = system_rand;
    break;
  }

  QVector<Point> points(numPoints);
  Point pt;

  bool failed = false;
  for (int i = 0; i < numPoints; ++i) {
    for (int j = 0; j < 3; ++j) {
      pt[j] = random_fn();
      failed = failed || (pt[j] < 0.0); // Check for negative numbers (errors)
    }
    points[i] = pt;
  }

  stop_demo();

  if (!failed) {
    m_ui->pointsLabel->setText(QString("%1 points.").arg(numPoints));
  }
  else {
    m_ui->pointsLabel->setText(QLatin1String("No points -- random number generator failed to read."));
    points.clear();
  }

  Wireframe *w = new Wireframe(points);
  m_gl_view->setWireframe(w);

  emit enableActionButtons(true);
}

/* vim: set et sw=2 ts=2: */
