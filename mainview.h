#ifndef _MAINVIEW_H
#define _MAINVIEW_H
/*
 * Copyright © 2009 Michael Pyne <mpyne@purinchu.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <QtGui/QWidget>

class QTimer;
class GLView;
namespace Ui { class MainView; };

class MainView : public QWidget
{
  Q_OBJECT
  public:

  MainView (QWidget *parent = 0);
  ~MainView ();

  signals:
  void enableActionButtons(bool enable);

  public slots:
  void clear_models ();
  void rotate ();
  void scale ();
  void start_demo ();
  void generate_data();

  private slots:
  void set_speed (int s);

  void play_demo_step ();
  void stop_demo ();

  private:
  GLView *m_gl_view;
  int m_speed;
  QTimer *m_demo;
  double last_t;
  Ui::MainView *m_ui;
};

#endif /* _MAINVIEW_H */

/* vim: set et sw=2 ts=2: */
