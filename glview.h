#ifndef _GLVIEW_H
#define _GLVIEW_H
/*
 * Copyright © 2009 Michael Pyne <mpyne@purinchu.net>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <QtOpenGL/QGLWidget>
#include "wireframe.h"

template<class T> class QList;
typedef QList<Wireframe *> WireframeList;

class GLView : public QGLWidget
{
  Q_OBJECT  // Needed for Qt

  public:
  GLView (QWidget *parent = 0);
  ~GLView ();

  void setWireframe(Wireframe *wireframe);
  Wireframe *wireframe() const { return m_wireframe; }

  virtual int heightForWidth (int w) const { return w; }
  virtual QSize sizeHint () const { return QSize (600, 600); }

  protected:
  virtual void paintGL();
  virtual void resizeGL (int width, int height);
  virtual void initializeGL ();

  private:
  void drawAxes (void);

  Wireframe *m_wireframe;
};

#endif

/* vim: set et sw=2 ts=2: */
